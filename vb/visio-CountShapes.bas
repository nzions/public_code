Attribute VB_Name = "Module1"
Public Sub CountShapes()
    Debug.Print "-------------"
    
    ' include the items in this array
    Dim include_strings(1 To 10) As String
    include_strings(1) = "GLC-T"
    include_strings(2) = "FET-40G"
    include_strings(3) = "SFP-10G-SR"
    include_strings(4) = "QSFP-H40G-AOC3M"
    include_strings(5) = "GLC-LH-SM"
    include_strings(6) = "FET-10G"
    

    Dim vsoPage As Page
    If Not (ActivePage Is Nothing) Then
        Set vsoPage = ActivePage
    Else
        Debug.Print "No active page"
    End If
    
    Dim resShape As Visio.Shape
    
    Dim intCounter As Integer
    Dim intCounter2 As Integer
    Dim intShapeCount As Integer
    Dim vsoShapes As Visio.Shapes

    Set vsoShapes = vsoPage.Shapes
    intShapeCount = vsoShapes.Count
    
    Dim shapeTextCount As Collection
    Set shapeTextCount = New Collection
    Dim shapeTextKeys As Collection
    Set shapeTextKeys = New Collection
    
    Dim shapeText As String
    
    If intShapeCount > 0 Then
        For intCounter = 1 To intShapeCount
         
            shapeText = vsoShapes.Item(intCounter).Text
            If shapeText = "" Then
                shapeText = "No Text"
            End If
            
            ' match the output shape
            If InStr(1, shapeText, "Item Counts", vbTextCompare) Then
                Set resShape = vsoShapes.Item(intCounter)
            End If
            
            Debug.Print vsoShapes.Item(intCounter).Name; " text:"; shapeText
            
            Dim continueok As Boolean
            continueok = False
            
            For intCounter2 = LBound(include_strings) To UBound(include_strings)
                If include_strings(intCounter2) = shapeText Then
                    continueok = True
                End If
            Next intCounter2
            
            If continueok = True Then
                If (CollectionContains(shapeTextCount, shapeText) = True) Then
                    Debug.Print "Increment "; shapeText
                    Dim cc As Integer
                    cc = shapeTextCount.Item(shapeText) + 1
                    shapeTextCount.Remove (shapeText)
                    shapeTextCount.Add cc, shapeText
                    
                Else
                    Debug.Print "Create "; shapeText
                    shapeTextCount.Add 1, shapeText
                    shapeTextKeys.Add shapeText, shapeText
                End If
            End If
        Next intCounter
        
        Dim message As String
        message = "Item Counts" & vbNewLine
        
        Dim clen As Integer
        clen = shapeTextKeys.Count
        For intCounter = 1 To clen
            Dim kval As String
            kval = shapeTextKeys.Item(intCounter)
            Debug.Print kval; shapeTextCount.Item(kval)
            message = message & kval & ": " & shapeTextCount.Item(kval) & vbNewLine
        Next intCounter

        MsgBox (message)
        If resShape Is Nothing Then
            Set resShape = vsoPage.DrawRectangle(0, 0, 2, 2)
        End If
        
        resShape.Text = message
        

    Else
        Debug.Print " No Shapes On Page"
    End If
End Sub
Public Function CollectionContains(col As Collection, key As Variant) As Boolean
    Dim obj As Variant
    On Error GoTo err
        CollectionContains = True
        obj = col(key)
        Exit Function
err:
        CollectionContains = False
End Function


