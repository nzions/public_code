Function cfn_NextIP(ip)
   ' a simple function which takes an ip address, and returns the next one along
    ' is not smart enough to go from 1.1.1.255 to 1.1.2.0

    Dim oct As Variant
    oct = Split(ip, ".")
    cfn_NextIP = oct(0) & "." & oct(1) & "." & oct(2) & "." & oct(3) + 1

End Function