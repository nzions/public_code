Function asearch( _
        lookup_value As String, _
        worksheet_match As String, _
        index_offset As Variant, _
        Optional match_type As String = "contains")

    ' array search
    ' searches your entire workbook for a value
    ' search is 'contains'
    ' returns a cell reference for use in other functions
    '
    ' asearch(value,worksheet match,index cols, match type)
    '   value is the plain text value searched for, can be a string or a cell ref
    '   worksheet match is the workseets which will be searched (match by match_type)
    '   match_type can be "contains" or "exact" (defaults to contains)
    '   index cols is which columns to search A=1
    '      can be a single col (1)
    '      or an array ({1,3,5})
    '
    ' asearch("foo","Sheet",1)
    ' asearch(B2, "srch_",{2,5})
    ' asearch(B2, "srch_",{2,5},"exact")
    '
    ' this is an array formula so you can us it as such
    ' don't forget the ctrl+shift+enter
    ' transpose() might be helpful too
    ' combine the results of this script with indirect() and offset() for some funky-ass vlookup-style action
    ' e.g. =OFFSET(INDIRECT(B9),0,-1,1,1)
    ' B9 containing the macro output, would return the cell to the left of the match
    ' e.g. =OFFSET(INDIRECT(B9),0,3,1,1)
    ' B9 containing the macro output, would return the cell 3rd to the right of the match
    '
    '
    ' Copyright Nikspace 2011
    ' Use only for good!
    '
    ' Version 0.3   Added match type functinoality (can now do match as contains or exact)
    ' Version 0.2   Fixed overflow bug at If (resultCount >= UBound(results)) Then
    ' Version 0.1   Initial Beta release
    '               it's probably buggy, don't blame me if it wipes your worksheet!
    '

    On Error GoTo eHandler

    Dim results(1 To 100) As Variant
    Dim i As Integer

    For n = 1 To UBound(results)
        results(n) = ""
    Next
    results(1) = ""
    ' results(1) = "No Results Found"

    ' index_offsets is an array
    Dim index_offsets() As Integer

    ' if just the integer is specified we have a 1 element array (Integer and Double)
    If (TypeName(index_offset) = "Integer") Then
        ReDim index_offsets(1 To 1) As Integer
        index_offsets(1) = index_offset
    ElseIf (TypeName(index_offset) = "Double") Then
        ReDim index_offsets(1 To 1) As Integer
        index_offsets(1) = index_offset

    ' otherwise copy the array across
    ElseIf (TypeName(index_offset) = "Variant()") Then
        ReDim index_offsets(1 To UBound(index_offset)) As Integer
        For i = 1 To UBound(index_offset)
            index_offsets(i) = index_offset(i)
        Next i
    Else
        results(1) = "ERROR! unsupported type passed as index_offset - " & TypeName(index_offset)
        GoTo fEnd
    End If
    
    ' check match type
    If match_type = "contains" Then
        ' all good
    ElseIf match_type = "exact" Then
        ' all good
    Else
        match_type = "contains"
    End If
    
        
    
    Dim ws As Worksheet
    Dim resultCount As Integer
    resultCount = 1

    For Each ws In ThisWorkbook.Worksheets
        If InStr(1, ws.Name, worksheet_match, vbTextCompare) Then
            Dim currentRow As Long
            Dim lastRow As Long

            lastRow = ws.UsedRange.Row - 1 + ws.UsedRange.Rows.Count
            For currentRow = 1 To lastRow

                ' break out if our resultCount hits the return array maximum
                ' 0.2 fixed overflow bug, should be >= not >
                If (resultCount >= UBound(results)) Then
                    GoTo fEnd
                End If

                For i = 1 To UBound(index_offsets)
                    ' 0.3 support for "contains" and "exact" searching
                    If match_type = "contains" Then
                        If InStr(1, ws.Cells(currentRow, index_offsets(i)).Value, lookup_value, vbTextCompare) Then
                            results(resultCount) = "'" & ws.Name & "'!" & int2col(index_offsets(i)) & currentRow
                            resultCount = resultCount + 1
                        End If
                    ElseIf match_type = "exact" Then
                        If ws.Cells(currentRow, index_offsets(i)).Value = lookup_value Then
                            results(resultCount) = "'" & ws.Name & "'!" & int2col(index_offsets(i)) & currentRow
                            resultCount = resultCount + 1
                        End If
                    End If
                Next i
            Next currentRow
        End If
    Next ws

    GoTo fEnd
    ' generic error handler
eHandler:
    MsgBox "Error in asearch " & Err.Number & "-" & Err.Description

fEnd:

    ' return the results
    asearch = results

End Function

Function col2int(gcol As String)
    ' converts a column to an int
    ' A is 1
    ' B is 2
    ' Z is 26
    ' ZZ is 702

    Dim ret As Integer
    Dim col As String
    col = gcol

    If (Len(col) = 1) Then
        ret = Asc(UCase(col)) - 64
    ElseIf (Len(col) = 2) Then
        ret = col2int(Mid(col, 2, 1))
        ret = ret + (col2int(Mid(col, 1, 1)) * 26)
    Else
        ret = CVErr(xlErrNA)
    End If

    col2int = ret
End Function
Function int2col(gi As Integer)
    ' converts an Integer to a Column
    ' 1 is A
    ' 26 is Z
    ' 27 is AZ
    ' can only do A to ZZ or 1 to 702

    Dim ret As String

    ' gi gets overwritten by the i=i-1 later, i thought variables here were local scope only
    ' i guess VB uses pass by ref
    Dim i As Integer
    i = gi

    ' we can only do to ZZ (702)
    If (i > 702) Then
        int2col = CVErr(xlErrNA)
        Return
    End If

    ' 0 is @... bad
    If (i = 0) Then
        int2col = CVErr(xlErrNA)
        Return
    End If

    ' excel starts off at 1, so we gotta subtract 1
    i = i - 1

    If (i > 25) Then
        Dim i1 As Integer
        Dim i2 As Integer
        i1 = i \ 26
        i2 = i - (i1 * 26)
        ret = Chr(i1 + 64) & Chr(i2 + 65)
    Else
        ret = Chr(i + 65)
    End If

    int2col = ret
End Function

