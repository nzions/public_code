' So I was surprised to learn how restrictive vlookup in excel was (boo microsoft... you suck!), so I wrote this replacement.
' The main thing was that vlookup does not support returning arrays, so you can't use it in an array formula.
' you also needed your data formated with the index column in the right place (what happens if your index column is z and you want to return column a?)
' Or... what if you want to search multiple "index" columns?
' behold... avlookupv... advanced(or array) vlookup vertical (I dunno... the name just popped into my head!)

Function avlookupv( _
        lookup_value As String, _
        table_array As Range, _
        index_offset As Variant, _
        return_offset As Integer, _
        nice_results As Boolean)

    ' avlookupv, Array vlookup vertical
    ' works kinda like vlookup, but returns an array so you can use it in array formulas
    ' you can also specify the index row and the return row (index_offset and return_offset)
    ' vlookup range_lookup isn't included, I have never needed it (always used FALSE) so this one works like FALSE (exact match)
    ' Bear in mind index and return offset is based on row 0, not the index row
    '
    ' use ctrl + shift + enter with multiple cells selected to use array output
    ' you might want to TRANSPOSE() for the results to go vertical instead of horizontal
    '
    '
    ' Examples
    ' Search vlookup style, index in col A, return Col C
    '
    ' =avlookupv("foo",A:C,1,3,FALSE)
    '
    '
    ' Search multiple Columns, return pretty results
    '   value to search for is in B5
    '   indexes are in D & F
    '   return column A
    '   pretty results
    '
    ' =avlookupv($B$5,A:F,{4,6},1,True)
    '
    '
    ' Copyright Nikspace 2010
    ' use only for good!
    '
    '
    ' Versoin 1.3       Fixed overflow bug at If (resultCount >= UBound(results)) Then
    '                   Made loop use lastRow

    ' Version 1.2a      Fixed bug with nice results and result 1
    '                   now is n/a when no results are found and nice_results is off

    ' Version 1.2       Sped up searching by detecting last row in sheet instead of counting spaces
    '                   If the return cell is empty return "" not 0
    '                   Added nice_results option which returns "prettier" results
    '                       "" instead of #n/a for no match
    '                       "No Results Found" in first val if there are no results
    ' Version 1.1       Addesd support for multiple index offsets
    '                   just pass them as an array in the formula
    '                   e.g. avlookupv("foo",A:Z,{2,4},3)
    ' Version 1.0       Original
    '
    '

    On Error GoTo eHandler

    ' define all the vars

    ' if you want more results change this line
    ' e.g. for 500 Dim results(1 To 500) As Variant
    Dim results(1 To 100) As Variant

    Dim resultCount As Integer
    Dim spaceCount As Integer
    Dim i As Integer

    ' don't change this! it's used to allocate the results, and vba arrays start at 1 not 0
    resultCount = 1

    ' Version 1.2 now checks for nice_results
    ' init the return array and other variables
    For n = 1 To UBound(results)
        If (nice_results = True) Then
            results(n) = ""
        Else
            results(n) = CVErr(xlErrNA)
        End If
    Next

    ' Version 1.2 nice_results "No Results Found"
    ' Version 1.2a
    If (nice_results = True) Then
        results(1) = "No Results Found"
    End If



    ' Version 1.1
    ' index_offsets is an array
    Dim index_offsets() As Integer

    ' if just the integer is specified we have a 1 element array (Integer and Double)
    If (TypeName(index_offset) = "Integer") Then
        ReDim index_offsets(1 To 1) As Integer
        index_offsets(1) = index_offset
    ElseIf (TypeName(index_offset) = "Double") Then
        ReDim index_offsets(1 To 1) As Integer
        index_offsets(1) = index_offset

    ' otherwise copy the array across
    ElseIf (TypeName(index_offset) = "Variant()") Then
        ReDim index_offsets(1 To UBound(index_offset)) As Integer
        For i = 1 To UBound(index_offset)
            index_offsets(i) = index_offset(i)
        Next i
    Else
        results(1) = "ERROR! unsupported type passed as index_offset - " & TypeName(index_offset)
        GoTo fEnd
    End If


    ' bail if index_offset is bigger than the columns in range
    ' Version 1.1 now using Array
    For i = 1 To UBound(index_offsets)
        If (index_offsets(i) > table_array.Columns.Count) Then
            results(1) = "ERROR! index_offset is outside the selected range, must be less than " & table_array.Columns.Count
            GoTo fEnd
        End If
    Next i

    ' bail if return_offset is bigger than the columns in range
    If (return_offsets > table_array.Columns.Count) Then
        results(1) = "ERROR! return_offset is outside the selected range, must be less than " & table_array.Columns.Count
        GoTo fEnd
    End If

    ' Version 1.2
    ' speed things up, find the last row so we don't search to 65535
    Dim lastRow As Long
    lastRow = table_array.Worksheet.UsedRange.Row - 1 + table_array.Worksheet.UsedRange.Rows.Count

    Dim currentRow As Long
    ' now load up the results
    For currentRow = 1 To lastRow
        ' Version 1.3 no longer needed loop only goes to new lastrow
        ' break out if we get to lastRow
        ' If (currentRow > lastRow) Then
        '    GoTo fEnd
        ' End If

        ' break out if our resultCount hits the return array maximum
        ' 1.3 fixed overflow bug, should be >= not >
        If (resultCount >= UBound(results)) Then
            GoTo fEnd
        End If

        ' on each row
        ' Version 1.1 - now array
        For i = 1 To UBound(index_offsets)
            With table_array.Cells(currentRow, index_offsets(i))
                ' do the search here
                If (.Value = lookup_value) Then

                    ' Version 1.2
                    ' if the cell is empty return "" not 0
                    If Not (table_array.Cells(currentRow, return_offset).Value = "") Then
                        results(resultCount) = table_array.Cells(currentRow, return_offset).Value
                    End If
                    resultCount = resultCount + 1
                End If
            End With
        Next i
    Next currentRow

    GoTo fEnd
    ' generic error handler
eHandler:
    MsgBox "Error in avlookupv " & Err.Number & "-" & Err.Description

fEnd:

    ' return the results
    avlookupv = results

End Function