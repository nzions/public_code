' This one has mostly been superceeded by avlookupv

Function avlookup( _
        lookup_value As String, _
        table_array As Range, _
        col_index_num As Integer)

    ' avlookup, Array vlookup
    ' works like vlookup, but returns an array so you can use it in array formulas
    ' vlookup range_lookup isn't included, I have never needed it (always used FALSE) so this one works like FALSE (exact match)
    '
    ' use ctrl + shift + enter with multiple cells selected to use array output
    ' you might want to TRANSPOSE() for the results to go vertical instead of horizontal
    '
    ' Copyright Nikspace 2010
    ' use only for good!
    '
    ' Version 1.0       Original
    '

    On Error GoTo eHandler

    ' define all the vars

    ' if you want more results change this line
    ' e.g. for 500 Dim results(1 To 500) As Variant

    Dim results(1 To 10) As Variant

    Dim resultCount As Integer
    Dim spaceCount As Integer

    ' init the return array and other variables
    For n = 1 To UBound(results)
        results(n) = CVErr(xlErrNA)
    Next

    resultCount = 1
    spaceCount = 1

    ' bail if the offset is bigget than the columns in the range
    If (col_index_num > table_array.Columns.Count) Then
        GoTo fEnd
    End If

    Dim lngRow As Long
    ' now load up the results
    For lngRow = 1 To table_array.Rows.Count
        ' break out if we get 100 empty cells in a row
        If (spaceCount > 100) Then
            GoTo fEnd
        End If

        ' break out if our resultCount hits the return array maximum
        If (resultCount > UBound(results)) Then
            GoTo fEnd
        End If

        ' on each row
        With table_array.Cells(lngRow, 1)
            ' do the search here
            If (.Value = lookup_value) Then
                results(resultCount) = table_array.Cells(lngRow, col_index_num).Value
                resultCount = resultCount + 1
            End If

            ' count the blank cells
            If (.Value = "") Then
                spaceCount = spaceCount + 1
            Else
                spaceCount = 1
            End If
        End With
    Next lngRow

    GoTo fEnd



    ' generic error handler
eHandler:
    MsgBox "There was an Error " & Err.Number & "-" & Err.Description

fEnd:

    ' return the results
    avlookup = results

End Function