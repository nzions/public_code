#!/bin/sh
#
# Simple Script to create an initial ramdisk
#

[ `id -u` -eq 0 ] || { echo "must be root" ; exit 1; }
[ "$1" == "" ] && { echo "Usage: $0 <root directory>" ; exit 1; }
[ -d $1 ] || { echo "$1 does not exist"; exit 1;}

cd $1
find | cpio -o -H newc | gzip -2 > ../$1.gz
cd ..