#!/bin/sh
#
# Simple Script to extract an initial ramdisk
#

[ `id -u` -eq 0 ] || { echo "must be root" ; exit 1; }
[ "$1" == "" ] && { echo "Usage: $0 <initrd.gz>" ; exit 1; }
echo $1 | grep -q ".gz$" || { echo "not a .gz" ; exit 1; }
[ -e $1 ] || { echo "$1 does not exist"; exit 1;}

directory=`echo $1 | sed 's/.gz$//'`
mkdir $directory
cd $directory
zcat ../$1 | cpio -i -H newc -d
cd ..