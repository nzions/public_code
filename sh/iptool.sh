#!/bin/sh
# Basic IP tool
# functions can be stripped out and used in other scripts
# Don't use for evil!
#
# base and broadcast are calculated 8 bits at a time
# ash uses signed 32 bit integers, so the largest possible int is 2147483647
# so this means generating the int above 127.255.255.255 will result in a negative int
# beware!
#
# tested on busybox ash v1.17.2
#
# Version 1.3   Removed echo in signed 32 bit int test
# Version 1.2   Converted expr's to $(()))
# Version 1.1   Clarified signed 32 bit int issues
# Version 1.0   Initial Release
#


usage() {
        echo "Usage: $0 <ip> <mask>"
        exit 1
}

[ -z $1 ] && usage
[ -z $2 ] && usage

ip2int() {

        [ -z $1 ] && return


        i4="${1##*.}" ; r="${1%.*}"
        i3="${r##*.}" ; r="${r%.*}"
        i2="${r##*.}" ; r="${r%.*}"
        i1="${r##*.}"

        s1="$(($i1*16777216))"
        s2="$(($i2*65536))"
        s3="$(($i3*256))"
        echo "$((s1+$s2+$s3+$i4))"
}

int2ip() {
        [ -z $1 ] && return

        oct1="$(($1/16777216))"
        amt="$((oct1*16777216))"
        rem="$(($1-$amt))"

        oct2="$(($rem/65536))"
        amt="$(($oct2*65536))"
        rem="$(($rem-$amt))"

        oct3="$(($rem/256))"
        amt="$(($oct2*256))"
        rem="$(($rem-$amt))"

        rem="$(($rem-$amt))"

        echo "$oct1.$oct2.$oct3.$rem"
}



getbase() {
        i4="${1##*.}" ; r="${1%.*}"
        i3="${r##*.}" ; r="${r%.*}"
        i2="${r##*.}" ; r="${r%.*}"
        i1="${r##*.}"

        m4="${2##*.}" ; r="${2%.*}"
        m3="${r##*.}" ; r="${r%.*}"
        m2="${r##*.}" ; r="${r%.*}"
        m1="${r##*.}"

        echo "$(($i1&$m1)).$(($i2&$m2)).$(($i3&$m3)).$(($i4&$m4))"
}
getbroadcast() {
        i4="${1##*.}" ; r="${1%.*}"
        i3="${r##*.}" ; r="${r%.*}"
        i2="${r##*.}" ; r="${r%.*}"
        i1="${r##*.}"

        m4="${2##*.}" ; r="${2%.*}"
        m3="${r##*.}" ; r="${r%.*}"
        m2="${r##*.}" ; r="${r%.*}"
        m1="${r##*.}"

        echo "$(($i1|(255-$m1))).$(($i2|(255-$m2))).$(($i3|(255-$m3))).$(($i4|(255-$m4)))"
}


if [ $((2147483647+1)) -eq -2147483648 ]; then
        echo "WARNING your shell uses signed 32 bit int's" >&2
        echo "WARNING Can't calulate integers for IP's above 127.255.255.255" >&2
fi

echo "IP: $1"
echo "Mask: $2"

echo "IP Int: `ip2int $1`"
echo "Mask Int: `ip2int $2`"

echo "Base: `getbase $1 $2`"
echo "Bcast: `getbroadcast $1 $2`"