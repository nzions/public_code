# public_code  
  
  
This repo contains the code previously hosted on nikspace.net in the codechunks section  
  
+ Unless otherwise noted I retain all Copyright and IP on everything herein.  
+ You may use it yourself, but I provide no guarantees or warranties.  
+ You should assume all code is buggy.  
+ If you use a piece of code here and it deletes everything, too bad, you should know better.  
+ If you use my code and make money off it you are required to negotiate a license and pay me.  
  
  
Contents:  
  
java/		Java Code, Mostly GWT related  
ruby/		Ruby code, mostly old and unmaintained  
sh/			Shell Scripts, usually bourne or bash  
vb/			Microsoft VB, mostly Excel Macro's (VBA)  
