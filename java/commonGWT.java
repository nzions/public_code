package com.ionsurge;

/*
 * don't forget to create a CommonGWT.gwt.xml file
 * mine contains:
 <module>
 <inherits name='com.google.gwt.user.User'/>
 <source path=''></source>
 <entry-point class='com.ionsurge.CommonGWT'/>
 </module>
 * 
 * Version 	1.0		Original Version
 * 			1.1		Cleaned up getURLParameter()
 */

import com.google.gwt.dom.client.Element;
import com.google.gwt.user.client.DOM;

public class CommonGWT {

	/*
	 * GWT needs this to compile
	 */
	public void onModuleLoad() {
		// i do nothing!
	}

	/*
	 * Completely Clear out a DOM element
	 */
	public static void domClear(Element parent) {
		com.google.gwt.user.client.Element firstChild;
		while ((firstChild = DOM.getFirstChild((com.google.gwt.user.client.Element) parent)) != null) {
			DOM.removeChild((com.google.gwt.user.client.Element) parent, firstChild);
		}
	}

	/*
	 * Gets a parameter from the URL e.g. ?abc=hello parameter is abc returns
	 * null if the parameter isn't specified in the URL
	 */
	public static String getURLParameter(String var) {
		return (com.google.gwt.user.client.Window.Location.getParameter(var));
	}

	/*
	 * JSNI method for getting the User Agent returns the User Agent in lower
	 * case
	 */
	public static native String getUserAgent() /*-{
		return navigator.userAgent.toLowerCase();
	}-*/;
}